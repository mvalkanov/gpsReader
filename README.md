# GPS Reader

A simple Android application to display GPS coordinates

## Build instructions
./gradlew clean assembleRelease

## APK is produced at app/build/outputs/apt/GpsReader*.apk
