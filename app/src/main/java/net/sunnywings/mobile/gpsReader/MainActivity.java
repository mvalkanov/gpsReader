/*
 * Copyright (c) 2016 Martin Valkanov. All rights reserved.
 */
package net.sunnywings.mobile.gpsReader;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
   private static final int REQUEST_CODE_ASK_PERMISSIONS = 123;

   private PlaceholderFragment placeholderFragment;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
      if (savedInstanceState == null) {
         placeholderFragment = new PlaceholderFragment();
         getSupportFragmentManager().beginTransaction()
               .add(R.id.container, placeholderFragment).commit();
      }
   }


   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.main, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();
      if (id == R.id.action_settings) {
         return true;
      }
      return super.onOptionsItemSelected(item);
   }

   @Override
   public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
      switch (requestCode) {
      case REQUEST_CODE_ASK_PERMISSIONS:
         if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (placeholderFragment != null) {
               placeholderFragment.initLocationListening();
            }
         }
         break;
      default:
         super.onRequestPermissionsResult(requestCode, permissions, grantResults);
      }
   }

   /**
    * A placeholder fragment containing a simple view.
    */
   public static class PlaceholderFragment extends Fragment implements
         LocationListener {
      private LocationManager locationManager;
      private TextView locationTextView;
      private String locationProviderType;
      private String locationProviderOnOff;
      private String locationProviderStatus;
      private String locationStr;

      public PlaceholderFragment() {
      }

      @Override
      public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
         View rootView =
               inflater.inflate(R.layout.fragment_main, container, false);
         locationTextView =
               (TextView) rootView.findViewById(R.id.location_text);

         initLocationListening();
         return rootView;
      }

      @Override
      public void onDestroyView() {
         stopLocationListening();
         super.onDestroyView();
      }

      protected void initLocationListening() {
         locationManager =
               (LocationManager) this.getActivity().getSystemService(
                     Context.LOCATION_SERVICE);
         locationProviderType = LocationManager.GPS_PROVIDER;
         locationProviderOnOff = "N/A";
         locationProviderStatus = "N/A";

         if (Build.VERSION.SDK_INT >= 23 &&
             ContextCompat.checkSelfPermission(this.getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
             ActivityCompat.requestPermissions(this.getActivity(), new String[] { android.Manifest.permission.ACCESS_FINE_LOCATION }, REQUEST_CODE_ASK_PERMISSIONS);
            return;
         }

         locationStr =
                  formatLocation(locationManager
                        .getLastKnownLocation(locationProviderType));
         locationManager.requestLocationUpdates(locationProviderType,
                  5000 /* 5 sec */, 10 /* meters */, this);

         refreshLocationText();
      }

      private void stopLocationListening() {
         if (Build.VERSION.SDK_INT >= 23 &&
             ContextCompat.checkSelfPermission(this.getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            return;
         }

        locationManager.removeUpdates(this);
      }

      private void refreshLocationText() {
         locationTextView.setText(locationProviderType + "\n"
               + locationProviderOnOff + "\n" + locationProviderStatus + "\n"
               + locationStr);
      }

      private String formatLocation(Location location) {
         String locationStr = "N/A";
         if (location != null) {
            locationStr =
                  "Lat: " + location.getLatitude() + " Long: "
                        + location.getLongitude() + " Alt: "
                        + location.getAltitude() + " m";
            Date date = new Date(location.getTime());
            SimpleDateFormat sdf =
                  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            locationStr += " at " + sdf.format(date);
         } else {
            locationStr = "N/A";
         }

         return locationStr;
      }

      @Override
      public void onLocationChanged(Location location) {
         locationStr = formatLocation(location);
         refreshLocationText();
      }

      @Override
      public void onProviderDisabled(String provider) {
         if (locationProviderType.equals(provider)) {
            locationProviderOnOff = "disabled";
         }
         refreshLocationText();
      }

      @Override
      public void onProviderEnabled(String provider) {
         if (locationProviderType.equals(provider)) {
            locationProviderOnOff = "enabled";
         }
         refreshLocationText();
      }

      @Override
      public void onStatusChanged(String provider, int status, Bundle extras) {
         if (!locationProviderType.equals(provider)) {
            return;
         }
         switch (status) {
         case LocationProvider.OUT_OF_SERVICE:
            locationProviderStatus = "out of service";
            break;
         case LocationProvider.TEMPORARILY_UNAVAILABLE:
            locationProviderStatus = "temporarily unavailable";
            break;
         case LocationProvider.AVAILABLE:
            locationProviderStatus = "available";
            break;
         default:
     .....  locationProviderStatus = "N/A";
            break;
         }

         refreshLocationText();
      }
   }
}
